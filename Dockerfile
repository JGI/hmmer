FROM debian:jessie
LABEL Maintainer Shijie Yao, syao@lbl.gov

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# create download and move into it
WORKDIR /workdir

# install the needed programs
ENV PACKAGES python wget zip 
RUN apt-get update && apt-get install --yes ${PACKAGES}

# download and install hummer 
ENV SRC http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz 
ENV TARFILE hmmer-3.1b2-linux-intel-x86_64.tar.gz 
ENV ZIP sparse.zip 

RUN wget ${SRC} && \
    tar -zxvf ${TARFILE} && \
    rm -rf ${TARFILE} && \
    mv hmmer-3.1b2-linux-intel-x86_64/binaries/* /usr/bin/. && \
    rm -rf hmmer-3.1b2-linux-intel-x86_64


