# HMMER docker image. #

HMMER is used for searching sequence databases for sequence homologs, and for making sequence alignments. 
It implements methods using probabilistic models called profile hidden Markov models (profile HMMs).

This image installed hmmer v3.1b2

hmmer executables are in program search path. To run:

docker run --rm --volume:/MY_PATH:/input hmmer:latest hmmsearch /input.globins4.hmm /input/globins45.fa